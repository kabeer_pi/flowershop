from django.db import models
from django.contrib.auth.models import AbstractUser,BaseUserManager
from django.contrib.auth.hashers import make_password

# Create your models here.

class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, username, email, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        if not username:
            raise ValueError('The given username must be set')
        email = self.normalize_email(email)
        username = self.model.normalize_username(username)
        user = self.model(username=username, email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(username, email, password, **extra_fields)

    def create_superuser(self, username, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(username, email, password, **extra_fields)

class User(AbstractUser):
    Email= models.EmailField(max_length=72,blank=True,null=True,unique=True)
    created_at = models.DateField(blank=True,null=True)
    First_Name = models.CharField(max_length=100,blank=True,null=True)
    Last_Name = models.CharField(max_length=100,blank=True,null=True)
    Password = models.CharField(max_length=100,blank=True,null=True)
    Address = models.CharField(max_length=100,blank=True,null=True)
    Postcode = models.IntegerField(blank=True,null=True)
    City = models.CharField(blank=True,null=True)
    Phone = models.IntegerField(blank=True,null=True)
    Vendor_ID = models.CharField(max_length=100,blank=True,null=True)

    USERNAME_FIELD = 'Email'
    REQUIRED_FIELDS = ['Address','Postcode','City','Phone']
    objects = UserManager()

    def get_username(self):
        return self.Email

class orders(models.Model):
    stat=('Delivered','Delivered'), ('Not_delivered','Not_delivered')
    Order_no = models.IntegerField(blank=True,null=True)
    Ordar_date = models.DateField(blank=True,null=True)
    Ordar_Total = models.IntegerField(blank=True,null=True)
    Customer_Id = models.ForeignKey(User,on_delete=models.CASCADE, null=True) 
    Delivery_Date = models.DateField(blank=True,null=True)
    Is_Delivered = models.CharField(max_length=25,choices=stat,null=True)
    Vendor_Id = models.CharField(max_length=25,blank=True,null=True) #models.ForeignKey(Vendor,on_delete=models.CASCADE, null=True)

    
class orderdetails(models.Model):
    Payment_due=('Paid','Paid'), ('Pending','Pending')
    Product_Id = models.CharField(max_length=25,blank=True,null=True) #models.ForeignKey(Products,on_delete=models.CASCADE, null=True)
    Product_Qty  = models.IntegerField(blank=True,null=True)
    Product_Price = models.IntegerField(blank=True,null=True)
    Order_Id = models.ForeignKey(orders,on_delete=models.CASCADE, null=True)
    Subtotal = models.IntegerField(blank=True,null=True)
    Payment_method = models.CharField(max_length=25,blank=True,null=True)
    Payment_Status = models.CharField(max_length=25,choices=Payment_due,null=True)
