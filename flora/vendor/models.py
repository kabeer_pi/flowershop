from django.db import models

# Create your models here.
class Vendor(models.Model):
    Vendor_Email = models.EmailField(max_length=72,blank=True,null=True)
    Vendor_branch = models.CharField(max_length=100,blank=True,null=True)
    Vendor_Name = models.CharField(max_length=100,blank=True,null=True)
    Vendor_Password = models.CharField(max_length=100,blank=True,null=True)
    Vendor_Address = models.CharField(max_length=100,blank=True,null=True)
    Vendor_Phone = models.IntegerField(blank=True,null=True)