from django.db import models

# Create your models here.

class Category(models.Model):
    Category_Name = models.CharField(max_length=100,blank=True,null=True)
    Category_Image = models.ImageField(upload_to='category_files/category',null=True, blank=True)
    Category_Description = models.CharField(max_length=1800,blank=True,null=True)
    Vendor_Id = models.CharField(max_length=25,blank=True,null=True) #models.ForeignKey(Vendor,on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.Category_Name

class Subcategory(models.Model):
    Subcategory_Name = models.CharField(max_length=100,blank=True,null=True)
    Subcategory_Image = models.ImageField(upload_to='category_files/Subcategory/',null=True, blank=True)
    Subcategory_Description  = models.CharField(max_length=1800,blank=True,null=True)
    Category_Id = models.ForeignKey(Category,on_delete=models.CASCADE, null=True)
    Vendor_Id = models.CharField(max_length=25,blank=True,null=True) #models.ForeignKey(Vendor,on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.Subcategory_Name

class Product(models.Model):
    stats=('In_stock','In_stock'), ('Out_of_stock','Out_of_stock')

    Product_Name = models.CharField(max_length=100,blank=True,null=True)
    Product_Image = models.ImageField(upload_to='Product_files/Primary/',null=True, blank=True)
    Product_gallery_image_1 = models.ImageField(upload_to='Product_files/Secondary/',null=True, blank=True)
    Product_gallery_image_2 = models.ImageField(upload_to='Product_files/Secondary/',null=True, blank=True)
    Product_gallery_image_3 = models.ImageField(upload_to='Product_files/Secondary/',null=True, blank=True)
    product_Description = models.CharField(max_length=1800,blank=True,null=True)
    price = models.IntegerField(blank=True,null=True)
    stock = models.IntegerField(blank=True,null=True)
    Category_Id = models.ForeignKey(Category,on_delete=models.CASCADE, null=True)
    Subcategory_Id = models.ForeignKey(Subcategory,on_delete=models.CASCADE, null=True)
    Stock_Status = models.CharField(max_length=25,choices=stats,null=True)
    Vendor_Id = models.CharField(max_length=25,blank=True,null=True) #models.ForeignKey(Vendor,on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.Product_Name
 



